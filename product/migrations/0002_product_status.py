# Generated by Django 3.0.1 on 2020-03-11 08:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='status',
            field=models.IntegerField(choices=[(1, 'Draft'), (2, 'Public'), (3, 'Hidden')], default=1, verbose_name='Status'),
        ),
    ]
