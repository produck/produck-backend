from rest_framework import serializers
from product.models import Product, ProductCategory, ProductImages
from tags.serializers import InlineTagSerializer


class InlineProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategory
        fields = (
            'url', 'name', 'description', 'template_icon'
        )
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }


class InlineProductImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImages
        fields = (
            'url', 'title', 'image', 'rank'
        )
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }


class ProductSerializer(serializers.ModelSerializer):
    category = InlineProductCategorySerializer(read_only=True)
    images = InlineProductImagesSerializer(many=True, read_only=True)
    tags = InlineTagSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = (
            'url', 'name', 'description', 'price', 'available_quantity', 'barcode', 'status',
            'images', 'category', 'tags', 'modified_at'
        )
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }


class InlineProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            'url', 'name', 'price', 'available_quantity', 'barcode', 'status', 'modified_at'
        )
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }


class ProductCategorySerializer(serializers.ModelSerializer):
    products = InlineProductSerializer(many=True, read_only=True)

    class Meta:
        model = ProductCategory
        fields = (
            'url', 'name', 'description', 'template_icon', 'products', 'modified_at'
        )
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }
