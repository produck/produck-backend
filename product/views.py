from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.filters import OrderingFilter
from rest_framework.filters import SearchFilter

from product.filters import ProductFilter
from product.models import Product, ProductCategory, StatusChoices, ProductImages
from product.serializers import ProductSerializer, ProductCategorySerializer, InlineProductImagesSerializer


class ProductCategoryViewSet(viewsets.ModelViewSet):
    queryset = ProductCategory.objects.all()
    serializer_class = ProductCategorySerializer
    filter_fields = (
        'status',
    )
    lookup_field = 'slug'

    def get_queryset(self):
        return ProductCategory.objects.all()


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.filter(status=StatusChoices.PUBLIC)
    serializer_class = ProductSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter,)
    filter_class = ProductFilter
    lookup_field = 'slug'
    search_fields = ('name',)
    ordering_fields = ('modified_at',)

    def get_queryset(self):
        return Product.objects.filter(status=StatusChoices.PUBLIC)


class ProductImagesViewSet(viewsets.ModelViewSet):
    queryset = ProductImages.objects.all()
    serializer_class = InlineProductImagesSerializer
    lookup_field = 'slug'

    def get_queryset(self):
        return ProductImages.objects.none()
