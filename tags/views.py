from rest_framework import viewsets

from tags.models import Tag
from tags.serializers import InlineTagSerializer


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = InlineTagSerializer
    lookup_field = 'slug'

    def get_queryset(self):
        return Tag.objects.all()
