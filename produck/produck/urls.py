from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from product.views import ProductViewSet, ProductCategoryViewSet, ProductImagesViewSet
from tags.views import TagViewSet


router = routers.DefaultRouter()
router.register(r'products', ProductViewSet)
router.register(r'categories', ProductCategoryViewSet)
router.register(r'images', ProductImagesViewSet)
router.register(r'tags', TagViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls'))
]
