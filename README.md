# produck-backend

Django API as the backend for the project

# Install

- Create a virtual environment: `virtualenv venv -p python3`
- Activate the virtual environment: `source venv/bin/activate`
- Install the project requirements: `pip install -r requirements.txt`
- Use example project in the `produck` folder: `cd produck`
- Migrate the database: `python manage.py migrate`
- Run the development site: `python manage.py runserver`
